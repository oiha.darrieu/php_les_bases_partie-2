<?php

// tableau en chaine separé d'un espace puis nettoyage des caractères blancs et remet en tableau chaque chaine de caractère
$strg = implode(' ', array_slice($argv, 1));

$arr2 = preg_split("/\s/", $strg, -1, PREG_SPLIT_NO_EMPTY);

// var_dump($arr2);

$arr_letter = [];
$arr_num = [];
$arr_other = [];

// echo $arr2[7][0]."\n";  //extrait le premier caractère de la veleur [7]
// echo substr($arr2[7] , 0 , 1)."\n"; //extrait le premier caractère de la valeur [7], on peut choisir la position de départ et le nombre de caractère à extraire

// trie des valeurs par le premier caractère dans 3 tableau différent : lettre, chiffre, autres
foreach ($arr2 as $value) {
    if (ctype_alpha($value[0])) {
        $arr_letter[] = $value;
    } elseif (is_numeric($value[0])) {
        $arr_num[] = $value;
    } else {
        $arr_other[] = $value;
    }
}

    sort($arr_other);
    natcasesort($arr_letter); // trie non sensible a la casse
    sort($arr_num, SORT_STRING); // trie les nombres comme une chaine de caracteres

// on fusionne les 3 tableaux
$arr_ALL = array_merge($arr_letter, $arr_num, $arr_other);
// on affiche le tableau combiné
foreach ($arr_ALL as $value) {
    echo "$value\n";
}
// var_dump(...$arr_ALL);
