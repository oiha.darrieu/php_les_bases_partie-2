<?php

if ($argc >= 2) {
    // valeur clé 1 en chaine de caractere
    $tab = preg_split("/\s/", $argv[1], -1, PREG_SPLIT_NO_EMPTY);

    // $FirstValue = array_shift($tab);
    // incrémente à la fin du tableau la première valeur tout en la supprimant
    $tab[] = array_shift($tab);

    // array_push($tab, $FirstValue);
    // affiche les éléments du tableau convertis en chaine de caractère séparé par un espace
    echo implode(' ', $tab) . "\n";
}
