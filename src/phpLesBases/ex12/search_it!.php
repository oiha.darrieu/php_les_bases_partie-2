<?php

if ($argc < 2) {
    exit;
}
$strg = implode(' ', array_slice($argv, 1));
$wordmaster = $argv[1];
$TOTOREX = "/^(?:)$wordmaster(?:)(?:(?:(?:\s)\w+:?(?:\w+)?(?:)){0,9}?(?:(?:\s)$wordmaster:?(?:\w+)?(?:)){0,9}?){0,9}?(?:)(?:\s)$wordmaster:(?<matchingkey>\w+)(?:)(?:\s\w+:?(\w+)?)?$/";
if (preg_match_all($TOTOREX, $strg, $arr1, PREG_SET_ORDER)) {
    echo $arr1[0]['matchingkey'] . "\n";
}
