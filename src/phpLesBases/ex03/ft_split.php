<?php

function ft_split(string $chaine)
{
    $tab = preg_split("/\W/", $chaine, -1, PREG_SPLIT_NO_EMPTY);
    sort($tab);

    return $tab;
}

//     $newtab = 0;
//     foreach ($tab as $elem) {
//         if ($elem != null) {
//             $newtab[] = $elem;
//         }
//     }

// sort ($newtab, SORT_STRING);
