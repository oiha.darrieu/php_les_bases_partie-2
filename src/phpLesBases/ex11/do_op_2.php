<?php

const SYNTAX_ERROR = 'Syntax Error';

function calcul($num1, $operator, $num2)
{
    switch ($operator) {
        case '+':
            return $num1 + $num2;
            break;
        case '-':
            return $num1 - $num2;
            break;
        case '*':
            return $num1 * $num2;
            break;
        case '/':
            return $num1 == 0 || $num2 == 0 ? 0 : ($num1 / $num2);
            break;
        case '%':
            return $num2 = 0 ? SYNTAX_ERROR : abs(fmod($num1, $num2));
            break;
    }
}
if ($argc != 2) {
    echo "Incorrect Parameters\n";  // s'il n'y pas qu'un 1 seul parametre à la commande, le program stop
    exit;
}
    $REGEXOSAURUS = "/^\s*(?<num1>[\+-]?\d*\.?\d+?)\s*(?<ope>\+|\*|\/|\-|\%)\s*(?<num2>(?1))\s*$/";

    if (!preg_match_all($REGEXOSAURUS, $argv[1], $arr1, PREG_SET_ORDER)) {
        echo SYNTAX_ERROR . "\n";
        exit;
    }
    echo calcul($arr1[0]['num1'], $arr1[0]['ope'], $arr1[0]['num2']) . "\n";
