<?php

const INVALID_PARAMETERS = "Incorrect Parameters\n";

if ($argc !== 4) {
    echo INVALID_PARAMETERS;
    exit;
}

$strg = implode(' ', array_slice($argv, 1));
$arr2 = preg_split("/\s/", $strg, -1, PREG_SPLIT_NO_EMPTY);

// echo calcul($arr2[0], $arr2[1], $arr2[2]);

if (!is_numeric($arr2[0]) || !is_numeric($arr2[2])) {
    echo INVALID_PARAMETERS;
    exit;
}
if ($arr2[1] == '+') {
    echo $arr2[0] + $arr2[2] . "\n";
} elseif ($arr2[1] == '/') {
    echo $arr2[2] == 0 || $arr2[0] == 0 ? "0\n" : $arr2[0] / $arr2[2] . "\n";
} elseif ($arr2[1] == '-') {
    echo $arr2[0] - $arr2[2] . "\n";
} elseif ($arr2[1] == '*') {
    echo $arr2[0] * $arr2[2] . "\n";
} elseif ($arr2[1] == '%' && $arr2[2] != 0) {
    echo $arr2[0] % $arr2[2] . "\n";
} else {
    echo INVALID_PARAMETERS;
}
